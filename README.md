# IHM project

front-end

[![front-end
status](https://gitlab.com/toliger/ihm-front/badges/master/pipeline.svg)](https://gitlab.com/toliger/ihm-front/commits/master)

api

[![api
status](https://gitlab.com/toliger/ihm-api/badges/master/pipeline.svg)](https://gitlab.com/toliger/ihm-api/commits/master)
## Play game

[game](https://ihm.oligertimothee.fr)

## Development

### Start the API

```sh
cd API/app && npm i && cd .. && docker-compose up
```

### Start the web engine

```sh
cd Front-end/app && npm i && npm run dev
```

# Build App

```js
nativefier --name "Code Names" "https://ihm.oligertimothee.fr/
```
