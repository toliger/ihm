# Lancement de l'application

## Pré-requis

* Docker
* docker-compose (facultatif)
* NodeJS (facultatif, uniquement pour le mode dev)

## Lancement de l'application (distribution)

C'est une application web, donc rendez-vous [>ici<](https://ihm.oligertimothee.fr) !

## Lancement de l'application (localement)

L'application côté client, une fois lancée, est accessible localement sur le port 8080 (http://localhost:8080)

### Client uniquement (utilise l'API hébergée en ligne)

#### 1) docker-compose est installé

```
docker-compose up
```

#### 2) docker-compose n'est pas installé sur la machine

Il suffit de pull l'image docker pour lancer l'application

```
docker run -p 8080:80 registry.gitlab.com/toliger/ihm-front:latest
```

Ou alors simplement

```
sh run.sh
```

### Client & API

#### Client

```
cd Front-End/app
npm i && npm run dev
```

#### API

Manière classique (de développement)

```
cd API/app
npm i
cd ..
docker-compose up
```

Directement via Docker

```
docker run -p 8081:8081 registry.gitlab.com/toliger/ihm-api:latest
```

## Remarques

Pour tester, il faut lancer 4 navigateurs (ou 2 en mode normal + leur équivalent en mode privé). Vous pouvez aussi proposer à des amis / collègues, c'est plus fun !
